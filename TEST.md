# commonmark 单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|          接口名          |是否通过	|备注|
|:---------------------:|:---:|:---:|
|  appendChild(child)   |    pass        |       |
|  prependChild(child)  |pass   |        |
|       unlink()        |pass   |        |
| insertAfter(sibling)  |pass   |        |
| insertBefore(sibling) |pass   |        |
|       walker()        |pass   |        |
|       Parser()        |pass  |     |
|     parse(input)      |   pass  |          |
|     XmlRenderer()     | pass |  |
|    HtmlRenderer()     | pass  |       |
|     render(node)      |  pass |          |
|        next()         |  pass |          |
|       resumeAt(node, entering)        | pass  |          |
